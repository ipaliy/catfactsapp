# CatFactsApp
**CatFactsApp** is a React Native application that keeps user posted about the cat facts.

This project was generated using [Expo](https://docs.expo.io/).

# Generate an application#
Run `expo start` to generate an application.

# API#
Using [Cat Facts](https://alexwohlbruck.github.io/cat-facts/) API 


# Further help#
Visit the [Expo Documentation](https://docs.expo.io/) to learn more.

