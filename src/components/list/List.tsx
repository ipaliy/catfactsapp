import React from 'react';
import { View } from 'react-native';
import { Fact } from '../../models/Fact';
import ListItem from '../list-item/ListItem';
import { styles } from './styles';

interface ListProps {
   data: Fact[]
}

export const List = (props: ListProps) => {

    const { data } = props;
    const showList = () => data.map((item, index)=>
            <ListItem key={index} title={item.text}/>
        );

    return (
            <View style={styles.container}>
                {showList()}
            </View>
    );
};

export default List;