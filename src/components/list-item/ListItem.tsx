import React, { useEffect, useRef } from 'react';
import { Animated, Text } from 'react-native';
import IconPaw from '../../assets/icons/IconPaw';
import { styles } from './styles';

interface ListItemProps {
   title: string
}

export const ListItem = (props: ListItemProps) => {
    
    const fadeAnim = useRef(new Animated.Value(0)).current;
    useEffect(() => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 1,
            duration: 500,
            useNativeDriver: true
          }
        ).start();
      }, [fadeAnim]);

    return (
            <Animated.View style={{ ...styles.container, opacity: fadeAnim }}>
                <IconPaw/>
                <Text style={styles.text}>{props.title}</Text>
            </Animated.View>
    );
};

export default ListItem;