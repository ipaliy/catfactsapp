import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        width: '95%',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 20,
        paddingRight: 40,
        paddingVertical: 15,
        marginVertical: 5,
        shadowColor: 'rgba(0,39,75,0.9)',
        shadowOffset: {
            width: 0,
            height: 3,
            },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 2,
        borderRadius: 8
    },
    text: {
        color: '#212121',
        alignSelf: 'center',
        marginLeft: 10
    }
  });