import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';

interface LoadButtonProps {
    title: string,
    display: boolean,
    onPress: () => void
}

export const LoadButton = (props: LoadButtonProps) => {
    return (
        <>
            {props.display&&<TouchableOpacity 
                style={styles.container}
                onPress={props.onPress}
                >
                <Text style={styles.text}>{props.title}</Text>
            </TouchableOpacity>}
        </>
    );
};

export default LoadButton;