import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    safeArea: {
        backgroundColor: '#2196F3'
    },
    topBar: {
        backgroundColor: '#2196F3',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
    },
    topBarTitle: {
        color: '#FFFFFF',
        fontFamily: 'Pacifico_400Regular',
        fontSize: 25
    }
  });