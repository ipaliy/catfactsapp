import React from 'react';
import { Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { styles } from './styles';

interface LayoutProps {
    children: JSX.Element;
}

export const Layout = (props: LayoutProps) => {
    const insets= useSafeAreaInsets();
    return (
            <>
                <View style={{ ...styles.safeArea, paddingTop: insets.top }}/>
                    <View style={styles.topBar}>
                        <Text style={styles.topBarTitle}>Cat App</Text>
                    </View>
                    {props.children}
            </>
    );
};

export default Layout;