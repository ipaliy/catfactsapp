export interface Fact {
    text: string,
    type: string,
    upvotes: number,
    user: any,
    userUpvoted: string,
    _id: string
  }