import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      height: '100%',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: 40,
      paddingTop: 20
    },
    listContainer: {
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    indicator: {
      flex: 1,
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center'
    }
  });