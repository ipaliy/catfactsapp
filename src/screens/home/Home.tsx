import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, ScrollView, View } from 'react-native';
import API from '../../api/api';
import Button from '../../components/button/Button';
import List from '../../components/list/List';
import { Fact } from '../../models/Fact';
import { styles } from './styles';

interface HomeProps {

}

export const Home = (props: HomeProps) => {

    const [dataLoaded, setDataLoaded] = useState(false);
    const [totalFacts, setTotalFacts] = useState<Fact[]>([]);
    const [facts, setFacts] = useState<Fact[]>([]);
    const [showButton, setShowButton] = useState(true);
    const list = useRef(null);

    useEffect(()=>{
        API.get('/facts')
        .then((response) => {
            setTotalFacts(response.data.all);
            setFacts(response.data.all.splice(0, 3));
        }, (error) => {console.log(error);})
        .then(()=>setDataLoaded(true));
    }, []);

    const handlePress = () => {
        const newFacts = totalFacts.splice(0, 3);
        setFacts([...facts, ...newFacts]);
        if (totalFacts.length === 0) {
            setShowButton(false);
        }
    };

    return (
        <>
        {!dataLoaded ? 
            <View style={styles.indicator}>
                <ActivityIndicator size="large"/>
            </View> 
            :
            <ScrollView 
                ref={list}
                onContentSizeChange={() => list.current.scrollToEnd()}
                >
                <View style={styles.container}>
                    <View style={styles.listContainer}>
                        <List data={facts}/>
                    </View>
                    <Button 
                        onPress={handlePress}
                        title={'Load more'}
                        display={showButton}
                        />
                </View>
            </ScrollView>
        }
        </>
    );
};

export default Home;