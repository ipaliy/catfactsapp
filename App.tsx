import { Lato_700Bold } from '@expo-google-fonts/lato';
import { Pacifico_400Regular, useFonts } from '@expo-google-fonts/pacifico';
import { AppLoading } from 'expo';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Layout from './src/components/layout/Layout';
import Home from './src/screens/home/Home';

export default function App() {

  let [fontsLoaded] = useFonts({
    Pacifico_400Regular,
    Lato_700Bold
  });
  
  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
      <SafeAreaProvider>
        <Layout>
          <Home/>
        </Layout>
      </SafeAreaProvider>
    );
}